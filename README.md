aws-elasticache-cluster-client-memcached-for-php compiled for centos as per:

https://github.com/awslabs/aws-elasticache-cluster-client-memcached-for-php/issues/11

```
yum update
yum install libevent-devel
yum remove php71-pecl-memcache php71-pecl-memcached

mkdir /tmp/libmemcached
cd /tmp/libmemcached
git clone https://github.com/awslabs/aws-elasticache-cluster-client-libmemcached.git
cd aws-elasticache-cluster-client-libmemcached
git checkout fb9a57250aebca26653ff4b294fd2377ae1eb0f1
mkdir BUILD
cd BUILD
../configure --prefix=/tmp/libmemcached --with-pic
make
make install

cd /usr/lib64
ln libsasl2.so.2 libsasl2.so.3
ln libsasl2.so.2 libsasl2.so

cd /tmp
git clone https://github.com/awslabs/aws-elasticache-cluster-client-memcached-for-php.git
cd aws-elasticache-cluster-client-memcached-for-php/
git checkout php7
phpize
./configure --with-libmemcached-dir=/tmp/libmemcached --disable-memcached-sasl
make
make install
```

